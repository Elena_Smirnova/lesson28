﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HW28
{
    public static class ArrayLib
    {
        public static Int32[] CreateArray(long count)
        {
            int[] array = new int[count];

            for(int i=0; i<array.Length;i++)
                array[i] = 1;

            return array;
        }

        /// <summary>
        /// sequential calculation 
        /// </summary>
        /// <param name="values">Int32 []</param>
        /// <returns>sum of array elements</returns>
        public static long SumСalculation(Int32[] values)
        {
            long result = 0;
            foreach (var value in values)
            {
                result += value;
            }

            return result;

        }

        public static long SumСalculation_2(Int32[] values)
        {
            return values.Sum();
        }


        /// <summary>
        /// parallel calculation 
        /// </summary>
        /// <param name="values">Int32 []</param>
        /// <returns>sum of array elements</returns>
        public static long TasksСalculation(Int32[] values, int thread)
        {
            IEnumerable<int[]> chanks = values.Chunk(values.Length / thread);

            Task<int>[] taskArray;
            taskArray = new Task<int>[chanks.Count()];
            int i = 0;
            foreach (int[] chank in chanks)
            {
                taskArray[i] = Task.Run(() => chank.Sum());
                ++i;
            }
            Task.WaitAll(taskArray);
            return taskArray.Sum(t => t.Result);
        }

        /// <summary>
        /// LINQ calculation 
        /// </summary>
        /// <param name="values">Int32 []</param>
        /// <returns>sum of array elements</returns>
        public static long LinqСalculation(Int32[] values, int thread)
        {
            return values.AsParallel().WithDegreeOfParallelism(thread).Sum();
        }


    }
}
