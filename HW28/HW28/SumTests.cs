﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW28
{
    public class SumTests
    {

        Stopwatch stopwatch = new Stopwatch();


        public void TestCalculation(Int32[] array, Func<Int32[],long> method)
        {
            stopwatch.Restart();
            var sum = method(array);
            stopwatch.Stop();

            Console.WriteLine($" sum = {sum}");
            Console.WriteLine($" {stopwatch.Elapsed.TotalMilliseconds}");
        }


        public void TestCalculation(Int32[] array, int thread, Func<Int32[], int, long> method)
        {

            stopwatch.Restart();
            var sum = method(array,thread);
            stopwatch.Stop();

            Console.WriteLine($" sum = {sum}");
            Console.WriteLine($" {stopwatch.Elapsed.TotalMilliseconds}");
        }


    }
}
