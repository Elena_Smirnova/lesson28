﻿using HW28;
using System.Diagnostics;


var array_1 = ArrayLib.CreateArray(100000);
var array_2 = ArrayLib.CreateArray(1000000);
var array_3 = ArrayLib.CreateArray(10000000);


Console.WriteLine("Default calculation");
new SumTests().TestCalculation(array_1, ArrayLib.SumСalculation);
new SumTests().TestCalculation(array_2, ArrayLib.SumСalculation);
new SumTests().TestCalculation(array_3, ArrayLib.SumСalculation);

Console.WriteLine("Task calculation");
new SumTests().TestCalculation(array_1, 6, ArrayLib.TasksСalculation);
new SumTests().TestCalculation(array_2, 6, ArrayLib.TasksСalculation);
new SumTests().TestCalculation(array_3, 6, ArrayLib.TasksСalculation);

Console.WriteLine("Linq calculation");
new SumTests().TestCalculation(array_1, 2, ArrayLib.LinqСalculation);
new SumTests().TestCalculation(array_2, 2, ArrayLib.LinqСalculation);
new SumTests().TestCalculation(array_3, 2, ArrayLib.LinqСalculation);





